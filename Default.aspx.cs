﻿/****************************** Module Header ******************************\
* Module Name:    Default.aspx.cs
* Project:        SimulatoreTasseUniversitarie
* Copyright (c) Microsoft Corporation
\*****************************************************************************/

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections.Specialized;
using System.Globalization;
using SimulatoreTasseUniversitarie.Properties;

namespace SimulatoreTasseUniversitarie
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #region CONFIGURAZIONE
        readonly double eurImpostaBollo = 16;
        readonly double eurTassaRegionaleDirittoStudio = 140;
        readonly double eurMinimaRiduzioneMeritevoli = 200;
        readonly int C_IseeMinimo_idx = 0;
        readonly int D_Aliquota_idx = 1;
        readonly int B_ContributoMinimoFascia_idx = 2;
        readonly int F_ContributoMassimo_idx = 3;
        readonly double perc_salvaguardia = 7;
        readonly double perc_salvaguardiaB_notA = 150;
        readonly bool Arrotondamento = true;
        readonly int numDecimaliValuta = 2;
        //readonly int ImportoNonEsigibile = 0; // euro. soglia del contributo onnicomprensivo sotto la quale non si esige il pagamento
        //readonly int ImportoAccorpamentoRate = 0; // euro. soglia sotto la quale l'importo della seconda e terza rata viene richiesto interamente nella seconda lasciando a zero la terza
        readonly double[,] Tabella1 = new double[,]{
            // (EURO)            (%)             (EURO)           (EURO)     
            // ISEE                            Contributo       Contributo
            // Minimo(C)      Aliquota(D)       minimo(B)          massimo
            {          0,            0.00,            240,             240},
            {      13000,            3.15,            240,             776},
            {      30000,            1.60,            776,             936},
            {      40000,            1.50,            936,            1086},
            {      50000,            1.40,           1086,            1226},
            {      60000,            1.30,           1226,            1551},
            {      85000,            0.00,           1551,            1551}
        };

        readonly string[] DescrizioneFascia = new string[] { "PRIMA", "SECONDA", "TERZA", "QUARTA", "QUINTA", "SESTA", "SETTIMA" };

        readonly Dictionary<string, double> TabellaCoefficientiCorso = new Dictionary<string, double> {
            {"coeffEco",1.00},
            {"coeffAgr",1.04},
            {"coeffSci",1.04},
            {"coeffMPS",1.15},
            {"coeffIng",1.18},
            {"coeffMed",1.59},
            {"coeffOdo",2.30}
        };
        #endregion  

        #region PARAMETRI PAGINA WEB
        protected String Get_PlaceHolderIsee()
        {
            String ret = "Importo ISEE per il diritto allo studio";
            bool IseeNonPresentato = Get_CheckboxValueFromPost("chkNoIsee");

            if (IseeNonPresentato)
            {
                ret = "ISEE per il diritto allo studio non presentato";
            }
            return ret;
        }

        protected String Get_FormPostDouble(String FormName)
        {
            string ci = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            String ret = "0" + ci + "0";
            try
            {
                string tmp = Request.Form[FormName];
                tmp = tmp.Replace(".", ci);
                tmp = tmp.Replace(",", ci);
                double test = double.Parse(tmp);

                ret = tmp;
            }
            catch (Exception)
            { ret = ""; }

            return ret;
        }

        protected String Get_FormPost(String FormName)
        {
            String ret = "";
            try
            {
                ret = Request.Form[FormName];
            }
            catch (Exception)
            { }

            return ret;
        }

        protected String Get_OptionPost(String FormName, String Option)
        {
            String ret = "";

            try
            {
                if (Request.Form[FormName] == Option)
                    ret = "selected";
            }
            catch (Exception)
            { }

            return ret;
        }

        protected String Get_FormPostCheckbox(String FormName)
        {
            bool ret = false;
            string check = "";
            try
            {
                if (Get_CheckboxValueFromPost(FormName))
                    ret = true;
            }
            catch (Exception)
            { }

            if (ret)
                check = " checked ";
            return check;
        }

        protected String Get_FormPostCheckboxForReduction(String FormName)
        {
            bool ret = false;
            string check = "";

            if (ViewReduction() != "none")
            {
                try
                {
                    if (Get_CheckboxValueFromPost(FormName))
                        ret = true;
                }
                catch (Exception)
                { }
            }

            if (ret)
                check = " checked ";
            return check;
        }

        protected String GetButtonLabel()
        {
            if (ViewReduction() == "none")
                return "CALCOLA TASSA";
            else
                return "RICALCOLA TASSA";
        }

        protected String ViewReduction()
        {
            String ret = "none";
            if (Page.IsPostBack)
            {
                double isee = Get_ValoreIsee();
                if (isee > -1 && isee <= Tabella1[2, C_IseeMinimo_idx]) // ISEE tra 0 e limite superiore seconda fascia: ISEE in primao seconda fascia
                {
                    ret = "block";
                }
            }

            return ret;
        }

        protected String ViewOpzionePrimoAnno()
        {
            String ret = "";
            if (Page.IsPostBack)
            {
                double isee = Get_ValoreIsee();
                if (isee > -1 && isee <= Tabella1[1, C_IseeMinimo_idx]) // ISEE tra 0 e il limite superiore prima fascia
                {
                    ret += "<div class=\"checkbox\">";
                    ret += "	<label>";
                    ret += "		<input type=\"checkbox\" name=\"RidPrimoAnno\" id=\"RidPrimoAnno\" onclick=\"UncheckRidAB();\" " + Get_FormPostCheckboxForReduction("RidPrimoAnno") + " />&nbsp;&nbsp;&nbsp;Studente iscritto al primo anno";
                    ret += "	</label>";
                    ret += "</div>";
                }
            }

            return ret;
        }

        protected String Get_IseeDisabled()
        {
            String ret = "";

            if (Page.IsPostBack && Get_CheckboxValueFromPost("chkNoIsee"))
            {
                ret = " disabled ";
            }

            return ret;
        }

        protected double Get_ValoreIsee()
        {
            double ret = 0;
            try
            {
                string tmp = Request.Form["A_ValoreIsee"];
                if (tmp == "")
                {
                    ret = -1;
                }

                string ci = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                tmp = tmp.Replace(".", ci);
                tmp = tmp.Replace(",", ci);
                ret = float.Parse(tmp);

                return ret;
            }
            catch (Exception)
            {
                ret = -1;
            }

            return ret;
        }

        protected String GoodParameters()
        {
            Boolean ParametersGood = true;

            if (Page.IsPostBack &&
                Get_ValoreIsee() < 0 &&
                !Get_CheckboxValueFromPost("chkNoIsee")) // importo ISEE non inserito e flag "ISEE non presentato" NON settato
            {
                ParametersGood = false;
            }

            return (ParametersGood ? "none" : "block");
        }

        protected Boolean Get_CheckboxValueFromPost(String fieldName)
        {
            return (Request.Form[fieldName] == "on") ? true : false;
        }
        #endregion

        #region FUNZIONI DI UTILITA'
        protected string GetScadenzaRata(int rata)
        {
            if (rata == 1)
                return Settings.Default["ScadenzaRata1"].ToString();

            if (rata == 2)
                return Settings.Default["ScadenzaRata2"].ToString();

            if (rata == 3)
                return Settings.Default["ScadenzaRata3"].ToString();

            return "";
        }

        protected string GetAnnoAccademico()
        {
            return Settings.Default["AnnoAccademico"].ToString();
        }

        protected void CalcolaRate(ref double rata_1, ref double rata_2, ref double rata_3, double TassaFinale, double CO_ContributoOnnicomprensivo)
        {
            double ImportoNonEsigibile = Double.Parse( Settings.Default["ImportoNonEsigibile"].ToString()); // euro. soglia del contributo onnicomprensivo sotto la quale non si esige il pagamento
            double ImportoAccorpamentoRate = Double.Parse(Settings.Default["ImportoAccorpamentoRate"].ToString()); // euro. soglia sotto la quale l'importo della seconda e terza rata viene richiesto interamente nella seconda lasciando a zero la terza
            int ImportoRataAccorpata = Int32.Parse(Settings.Default["ImportoRataAccorpata"].ToString()); // rata. indica la rata in cui accorpare l'importo della 2* e 3* rata. Es: 2--> accorpa il totale della 2* e 3* rata nella 2*

            rata_1 = eurImpostaBollo + eurTassaRegionaleDirittoStudio;
            rata_2 = (TassaFinale - rata_1) / 2;
            rata_3 = rata_2;

            if (CO_ContributoOnnicomprensivo <= ImportoNonEsigibile)
            {
                rata_2 = 0;
                rata_3 = 0;
            }
            else if (CO_ContributoOnnicomprensivo <= ImportoAccorpamentoRate)
            {
                if (ImportoRataAccorpata == 2)
                {
                    rata_2 = TassaFinale - rata_1; 
                    rata_3 = 0;
                }
                else
                {
                    rata_2 = 0;
                    rata_3 = TassaFinale - rata_1;
                }
            }
        }

        private String Euro(double importo, int num_dec)
        {
            String dec = "";
            if (num_dec > 0)
            {
                for (int i = 0; i < num_dec; i++)
                {
                    dec += "0";
                }

                dec = "." + dec;
            }

            dec = "{0:0" + dec + "}";

            return String.Format(dec, importo);
        }

        protected double ArrotondaConTroncamentoEuroSuperiore(double importo)
        {
            double importo_approssimato = 0;

            int TroncamentoEuroInferiore = (int)importo;
            if (importo != TroncamentoEuroInferiore)
            {
                importo_approssimato = TroncamentoEuroInferiore + (importo > 0 ? 1 : -1);
            }
            else
            {
                importo_approssimato = TroncamentoEuroInferiore;
            }

            return importo_approssimato;
        }

        private int GetFasciaReddito(double a_ValoreIsee, bool iseeNonPresentato)
        {
            int fascia = 0;
            int numeroFasce = Tabella1.GetLength(0);

            if (iseeNonPresentato)
            {
                fascia = numeroFasce - 1; // SE L'ISEE NON E' STATO PRESENTATO VIENE ATTRIBUITA AUTOMATICAMENTE LA FASCIA PIU' ALTA
                a_ValoreIsee = Tabella1[numeroFasce - 1, C_IseeMinimo_idx] + 1; // VALORE IN EURO DELLA FASCIA PIU' ALTA + 1 EURO
            }
            else if (a_ValoreIsee > 0)
            {
                for (fascia = 0; fascia < numeroFasce; fascia++)
                {
                    if (fascia < numeroFasce - 1) // DALLA PRIMA ALLA PENULTIMA FASCIA
                    {
                        if (a_ValoreIsee > Tabella1[fascia, C_IseeMinimo_idx] && a_ValoreIsee <= Tabella1[fascia + 1, C_IseeMinimo_idx])
                        {
                            break;
                        }
                    }
                    else if (a_ValoreIsee > Tabella1[fascia, C_IseeMinimo_idx]) // SOLO PER L'ULTIMA FASCIA
                    {
                        break;
                    }
                }
            }

            return fascia;
        }

        private Riduzione GetRiduzione(double a_ValoreIsee, bool iseeNonPresentato, bool ridPrimoAnno, bool ridMeritevoliA, bool ridMeritevoliB)
        {
            Riduzione TipoRiduzione = Riduzione.NessunaRiduzione;

            if (!iseeNonPresentato && (a_ValoreIsee <= Tabella1[1, C_IseeMinimo_idx])) // ISEE presentato e in prima fascia
            {
                if (ridPrimoAnno)
                {
                    //// NO TAX AREA
                    TipoRiduzione = Riduzione.NoTaxArea;
                }
                else if (ridMeritevoliA && ridMeritevoliB)
                {
                    //// NO TAX AREA
                    TipoRiduzione = Riduzione.NoTaxArea;
                }
                else if (!ridMeritevoliA && ridMeritevoliB)
                {
                    TipoRiduzione = Riduzione.Riduzione_B;
                }
            }

            else if (!iseeNonPresentato && (Tabella1[1, C_IseeMinimo_idx] < a_ValoreIsee && a_ValoreIsee <= Tabella1[2, C_IseeMinimo_idx])) // ISEE PRESENTATO E in SECONDA FASCIA
            {
                if (ridMeritevoliA && ridMeritevoliB)
                {
                    TipoRiduzione = Riduzione.Riduzione_A_and_B;
                }
                else if (!ridMeritevoliA && ridMeritevoliB)
                {
                    TipoRiduzione = Riduzione.Riduzione_B;
                }
            }

            return TipoRiduzione;
        }

        enum Riduzione
        {
            NessunaRiduzione,
            NoTaxArea,
            Riduzione_A_and_B,
            Riduzione_B
        };
        #endregion

        protected String CalcolaTassa()
        {
            double rata_1 = 0;
            double rata_2 = 0;
            double rata_3 = 0;
            String ret = "";
            String previsione_tassa = "";
            Riduzione TipoRiduzione = Riduzione.NessunaRiduzione;

            HTML_PrevisioneTassa(ref previsione_tassa);

            if (!Page.IsPostBack)
            {
                ret = previsione_tassa.Replace("[VALORE_PREVISIONE]", "");
                return ret;
            }
            try
            {
                double B, A, C, D, E, CO_ContributoOnnicomprensivo, TassaFinale;
                int fascia = 0;
                string strDettagli = "";
                string strDettaglioRiduzione = "";
                double A_ValoreIsee = Get_ValoreIsee();
                int numeroFasce = Tabella1.GetLength(0);

                bool IseeNonPresentato = Get_CheckboxValueFromPost("chkNoIsee");

                if (A_ValoreIsee < 0 && !IseeNonPresentato)
                {
                    ret = previsione_tassa.Replace("[VALORE_PREVISIONE]", "");
                    return ret;
                }

                ////// INPUT UTENTE
                bool RidMeritevoliA = Get_CheckboxValueFromPost("RidMeritevoliA");
                bool RidMeritevoliB = Get_CheckboxValueFromPost("RidMeritevoliB");
                bool RidPrimoAnno = Get_CheckboxValueFromPost("RidPrimoAnno");
                string E_CoefficienteCorso = Request.Form["E_CoefficienteCorso"];

                E = TabellaCoefficientiCorso[E_CoefficienteCorso];

                /// TROVA LA FASCIA IN BASE ALL'IMPORTO ISEE
                fascia = GetFasciaReddito(A_ValoreIsee, IseeNonPresentato);
                A = A_ValoreIsee;
                B = Tabella1[fascia, B_ContributoMinimoFascia_idx];
                C = Tabella1[fascia, C_IseeMinimo_idx];
                D = Tabella1[fascia, D_Aliquota_idx] / 100;

                CO_ContributoOnnicomprensivo = (B + (A - C) * D) * E;

                /////// TROVA IL TIPO DI RIDUZIONE
                TipoRiduzione = GetRiduzione(A_ValoreIsee, IseeNonPresentato, RidPrimoAnno, RidMeritevoliA, RidMeritevoliB);

                double CO_temp = CO_ContributoOnnicomprensivo;
                double eccedenza_prima_fascia = A_ValoreIsee - Tabella1[1, C_IseeMinimo_idx];
                if (eccedenza_prima_fascia < 0)
                    eccedenza_prima_fascia = 0;
                double limite_salvaguardia = (perc_salvaguardia * eccedenza_prima_fascia) / 100;
                double AmmontareRiduzione = 0;
                strDettaglioRiduzione = "";

                switch (TipoRiduzione)
                {
                    case Riduzione.NoTaxArea:
                        CO_temp = 0;
                        break;
                    case Riduzione.Riduzione_A_and_B:/// limite salvaguardia
                        if (CO_temp > limite_salvaguardia)
                            CO_temp = limite_salvaguardia;
                        break;
                    case Riduzione.Riduzione_B:/// limite salvaguardia + 50% , minimo 200
                        if (CO_temp > (limite_salvaguardia * perc_salvaguardiaB_notA) / 100)
                            CO_temp = (limite_salvaguardia * perc_salvaguardiaB_notA) / 100;

                        if (CO_temp < eurMinimaRiduzioneMeritevoli)
                            CO_temp = eurMinimaRiduzioneMeritevoli;
                        break;
                    case Riduzione.NessunaRiduzione:
                    default:
                        break;
                }

                if (Arrotondamento) ///// ARROTONDAMENTI
                {
                    CO_ContributoOnnicomprensivo = ArrotondaConTroncamentoEuroSuperiore(CO_ContributoOnnicomprensivo);
                    CO_temp = ArrotondaConTroncamentoEuroSuperiore(CO_temp);
                }

                AmmontareRiduzione = CO_ContributoOnnicomprensivo - CO_temp;
                if (AmmontareRiduzione < 0)
                {
                    AmmontareRiduzione = 0;
                }

                CO_ContributoOnnicomprensivo = CO_temp;

                if (AmmontareRiduzione > 0)
                {
                    strDettaglioRiduzione = " ( Riduzione di " + Euro(AmmontareRiduzione, numDecimaliValuta) + "&euro; )";
                }

                TassaFinale = CO_ContributoOnnicomprensivo +
                  eurImpostaBollo +
                  eurTassaRegionaleDirittoStudio;

                HTML_Init(ref strDettagli);
                HTML_ISEE(ref strDettagli, A_ValoreIsee);
                HTML_Tasse(ref strDettagli, CO_ContributoOnnicomprensivo, strDettaglioRiduzione);

                if (AmmontareRiduzione > 0)
                {
                    String motivoRiduzione = "";
                    switch (TipoRiduzione)
                    {
                        case Riduzione.NoTaxArea:
                            motivoRiduzione = "No Tax Area - Esonero totale";
                            break;
                        case Riduzione.Riduzione_A_and_B:
                        case Riduzione.Riduzione_B:
                            motivoRiduzione = "Studente meritevole";
                            break;
                        case Riduzione.NessunaRiduzione:
                        default:
                            break;
                    }

                    HTML_Riduzione(ref strDettagli, AmmontareRiduzione, motivoRiduzione);
                }

                CalcolaRate(ref rata_1, ref rata_2, ref rata_3, TassaFinale, CO_ContributoOnnicomprensivo);

                TassaFinale = rata_1 + rata_2 + rata_3;

                HTML_Rate(ref strDettagli, rata_1, rata_2, rata_3);

                ret = previsione_tassa.Replace("[VALORE_PREVISIONE]", Euro(TassaFinale, numDecimaliValuta));

                HTML_Container(ref ret);

                ret = ret.Replace("[---strDettaglioIsee innerHTML---]", strDettagli);
            }
            catch (Exception)
            {
                ret = previsione_tassa.Replace("[VALORE_PREVISIONE]", "");
            }

            return ret;
        }

        #region HTML
        private void HTML_PrevisioneTassa(ref string previsione_tassa)
        {
            previsione_tassa = "<div class=\"input-group\">";
            previsione_tassa += "  <span class=\"input-group-addon\"><b class=\"big-text\">&euro;*</b></span>";
            previsione_tassa += "  <input type = \"text\" name=\"strTassaFinale\" id=\"strTassaFinale\" value=\"[VALORE_PREVISIONE]\" class=\"form-control text-center big-text taller\" style=\"cursor:default;\" placeholder=\"Previsione Tassa\" disabled />";
            previsione_tassa += "</div>";
        }

        private void HTML_Init(ref string strDettagli)
        {
            strDettagli = "<p style=\"font-weight:normal; font-size:15px;\">";
            strDettagli += "<strong><u>Dettaglio Tassa</u></strong><br />";
        }

        private void HTML_ISEE(ref string strDettagli, double a_ValoreIsee)
        {
            if (a_ValoreIsee >= 0)
            {
                strDettagli += "Reddito ISEE per il diritto allo studio : " + Euro(a_ValoreIsee, numDecimaliValuta) + "&nbsp;&euro;" + "<br />";
            }
            else
            {
                strDettagli += "Reddito ISEE per il diritto allo studio : non dichiarato<br />";
            }
        }

        private void HTML_Tasse(ref string strDettagli, double co_ContributoOnnicomprensivo, string strDettaglioRiduzione)
        {
            strDettagli += "Imposta di bollo : " + Euro(eurImpostaBollo, numDecimaliValuta) + " &euro; <br />";
            strDettagli += "Tassa regionale per il diritto allo studio : " + Euro(eurTassaRegionaleDirittoStudio, numDecimaliValuta) + " &euro; <br />";
            strDettagli += "Contributo onnicomprensivo : " + Euro(co_ContributoOnnicomprensivo, numDecimaliValuta) + " &euro; " + strDettaglioRiduzione;
            strDettagli += "<br />";
        }

        private void HTML_Riduzione(ref string strDettagli, double ammontareRiduzione, string motivoRiduzione)
        {
            strDettagli += "<br />";
            strDettagli += "<strong><u>Riduzione del contributo onnicomprensivo</u></strong><br />";
            strDettagli += "Riduzione : " + motivoRiduzione + "<br />";
            strDettagli += "Ammontare riduzione : " + Euro(ammontareRiduzione, numDecimaliValuta) + "&nbsp;&euro;" + "<br />";
        }

        private void HTML_Rate(ref string strDettagli, double rata_1, double rata_2, double rata_3)
        {
            strDettagli += "<table class=\"table table-striped\">";
            strDettagli += "    <thead>";
            strDettagli += "        <tr>";
            strDettagli += "            <th># RATA</th>";
            strDettagli += "            <th>IMPORTO</th>";
            strDettagli += "            <th>SCADENZA</th>";
            strDettagli += "        </tr>";
            strDettagli += "    </thead>";
            strDettagli += "    <tbody>";
            strDettagli += "        <tr>";
            strDettagli += "            <th scope=\"row\">Prima</th>";
            strDettagli += "            <td>" + Euro(rata_1, numDecimaliValuta) + " &euro;</td>";
            strDettagli += "            <td>" + GetScadenzaRata(1) + "</td>"; 
            strDettagli += "        </tr>";
            strDettagli += "        <tr>";
            strDettagli += "            <th scope=\"row\">Seconda</th>";
            if (rata_2 == 0)
                strDettagli += "            <td>" + "Non dovuta." + "</td>";
            else
                strDettagli += "            <td>" + Euro(rata_2, numDecimaliValuta) + " &euro;" + "</td>";

            strDettagli += "            <td>" + GetScadenzaRata(2) + "</td>";
            strDettagli += "        </tr>";
            strDettagli += "        <tr>";
            strDettagli += "            <th scope=\"row\">Terza</th>";
            if (rata_3 == 0)
                strDettagli += "            <td>" + "Non dovuta." + "</td>";
            else
                strDettagli += "            <td>" + Euro(rata_3, numDecimaliValuta) + " &euro;" + "</td>";
            strDettagli += "            <td>" + GetScadenzaRata(3) + "</td>";
            strDettagli += "        </tr>";
            strDettagli += "    </tbody>";
            strDettagli += "</table>";
            strDettagli += "</p>";
        }

        private void HTML_Container(ref string ret)
        {
            ret += "<span id = \"ShowDettagli\" style = \"display:block\" >";
            ret += "    <br />";
            ret += "    <div class=\"interesting_area\" style=\"background-color:aliceblue;\">";
            ret += "        <div id = \"strDettaglioIsee\" style=\"font-style:italic;\">[---strDettaglioIsee innerHTML---]";
            ret += "        </div>";
            ret += "    </div>";
            ret += "</span>";
        }
        #endregion
    }
}