﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SimulatoreTasseUniversitarie.Default" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>UNIVPM: Simulatore Tasse</title>
    <link rel="stylesheet" type="text/css" href="css/stile.css" />
    <link rel="stylesheet" type="text/css" href="css/styles.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/custom.css" />
    <!--script type="text/javascript" src="js/bootstrap.min.js"></!--script-->
    <script type="text/javascript" src="js/custom.js" language="javascript" ></script>
</head>
<body style="padding-top: 10px;">
    <div class="header" style="margin:0px;padding: 10px;">
        <a href="http://www.univpm.it"><img class="pull-left" src="img/logo.png" style="width: 250px;" alt="Univpm" /></a>
        <label class="control-label text-right vcenter">
            <strong class="titolo_modulo">Calcolo Tasse&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
            <br />
            <strong class="sottotitolo_modulo">SIMULATORE CONTRIBUZIONE STUDENTESCA A.A. 2017/2018&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
        </label>

    </div><!-- end header -->

    <hr class="bbreak" />

    <div class="container-fluid">

    <br /><br />

    <div class="col-sm-12">
        <div class="well">
            <form id="PrevisioneTassa" name="PrevisioneTassa" runat="server" method="post" action="Default.aspx#bottom">

                <label>Importo ISEE per il diritto allo studio</label>
                <br />
                <div class="input-group">
                    <span class="input-group-addon"><b class="big-text">&euro;</b></span>
                    <input type="text" onkeypress="return OnlyNumbers(event)" id="A_ValoreIsee" name="A_ValoreIsee" class="form-control text-center big-text taller" placeholder="Inserisci l'importo del tuo ISEE per il diritto allo studio" value="<%=Get_FormPostDouble("A_ValoreIsee") %>" <%=Get_IseeDisabled() %>/>
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" style="margin-top: 0px;" name="chkNoIsee" id="chkNoIsee"  onclick="IseeNonPresentato();" <%=Get_FormPostCheckbox("chkNoIsee") %>/><span class="help-block" style="margin-top: -2px;">ISEE NON PRESENTATO</span>
                    </label>
                </div>



                <label>Corso di studio</label>
                <select  class="form-control taller" name="E_CoefficienteCorso" id="E_CoefficienteCorso">
                    <option value="coeffEco" <%=Get_OptionPost("E_CoefficienteCorso", "coeffEco") %> >ECONOMIA</option> 
                    <option value="coeffAgr" <%=Get_OptionPost("E_CoefficienteCorso", "coeffAgr") %> >AGRARIA</option>
                    <option value="coeffSci" <%=Get_OptionPost("E_CoefficienteCorso", "coeffSci") %> >SCIENZE</option>
                    <option value="coeffMPS" <%=Get_OptionPost("E_CoefficienteCorso", "coeffMPS") %> >MEDICINA PROFESSIONI SANITARIE</option>
                    <option value="coeffIng" <%=Get_OptionPost("E_CoefficienteCorso", "coeffIng") %> >INGEGNERIA</option>
                    <option value="coeffMed" <%=Get_OptionPost("E_CoefficienteCorso", "coeffMed") %> >corso di laurea MEDICINA E CHIRURGIA</option>
                    <option value="coeffOdo" <%=Get_OptionPost("E_CoefficienteCorso", "coeffOdo") %> >corso di laurea ODONTOIATRIA E P.D.</option>
                </select>

                <br />




                <a id="bottom"></a>
                <label>PREVISIONE TASSA</label>
                <div class="interesting_area_highlight">


                    <%=CalcolaTassa() %>

                </div>
                <h5 class="text-center">* L&rsquo;esito della simulazione ha valore puramente indicativo e non costituisce certificazione.</h5>
                <br />

                                
                

                <div class="interesting_area" style="background:none; border:0px;text-align:center">
                    
                    <input type="submit" style="font-size:2em;" class="btn btn-primary" value="&nbsp;&nbsp;&nbsp;<%=GetButtonLabel() %>&nbsp;&nbsp;&nbsp;" />
                </div>
                <br />

                <div class="interesting_area" style="text-align:center; display:<%=GoodParameters() %>;">
                    <strong>Prego,<br />controllare i dati inseriti</strong>
                </div>



                <div class="interesting_area" style="display:<%=ViewReduction() %>;">
                    <strong><br />Con i parametri inseriti si potrebbe avere diritto ad una riduzione del contributo onnicomprensivo. 
                        <br />Prego compilare anche la sezione sottostante e ricalcolare la tassa.<br /><br />
                    </strong>
                </div>
                







                <div id="RiduzioneTassa" style="display:<%=ViewReduction() %>;">
                <label><!--NO TAX AREA--><br /></!--label>
                <div class="interesting_area">
                    <div class="checkbox">
                        <label style="display:<%=ViewOpzionePrimoAnno() %>;">
                            <input type="checkbox" name="RidPrimoAnno" id="RidPrimoAnno" onclick="UncheckRidAB();" <%=Get_FormPostCheckboxForReduction("RidPrimoAnno") %> />&nbsp;&nbsp;&nbsp;Studente iscritto al primo anno
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="RidMeritevoliA" id="RidMeritevoliA" onclick="UncheckRidPrimoAnno();" <%=Get_FormPostCheckboxForReduction("RidMeritevoliA") %> />&nbsp;&nbsp;&nbsp;Studente iscritto all&rsquo;Universit&agrave; Politecnica delle Marche da un numero di anni accademici inferiore o uguale alla durata normale del corso di studio aumentata di uno;
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="RidMeritevoliB" id="RidMeritevoliB" onclick="UncheckRidPrimoAnno();" <%=Get_FormPostCheckboxForReduction("RidMeritevoliB") %> />&nbsp;&nbsp;&nbsp;Studente che, nel caso di iscrizione al secondo abbia conseguito entro il 10 agosto del primo anno, almeno 10 CFU; <b>oppure</b>, nel caso di iscrizione ad anni accademici successivi al secondo abbia conseguito, nei dodici mesi antecedenti la data del 10 agosto precedente la relativa iscrizione, almeno 25 CFU.
                        </label>
                    </div>
                </div>
                </div>

                <br />


            </form>

        </div><!-- end well -->


    </div><!-- end col-sm-12 -->
    <br /><br /><br />

    </div><!-- end container-fluid -->

    <div class="contact">
      <strong>Universit&agrave; Politecnica delle Marche</strong> 
      P.zza Roma 22, 60121 Ancona 
      Tel (+39) 071.220.1, 
      Fax (+39) 071.220.2324 
      P.I. 00382520427
    </div>

</body>
</html>
