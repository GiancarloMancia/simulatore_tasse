<!--
var Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('');


function escapeRegExp(str)
{
  return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}
function replaceAll(str, find, replace)
{
  return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function OnlyNumbers(evt)
{
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode >= 48 && charCode <= 57) || charCode==46 || charCode==44)
    {
        return true;
    }
    return false;
}


function CoerceInRange(Num, Min, Max)
{
    var ret = Num;
    
    if (ret>Max)
        ret = Max;
        
    if (ret<Min)
        ret = Min;
    
    return ret;
    //return Math.min(Math.max(Num, Min), Max);
}

function UncheckRidAB ()
{
	if (document.getElementById("RidPrimoAnno").checked)
	{
		document.getElementById("RidMeritevoliA").checked = false;
		document.getElementById("RidMeritevoliB").checked = false;
	}
}

function UncheckRidPrimoAnno ()
{
	try
	{
		if (document.getElementById("RidMeritevoliA").checked || document.getElementById("RidMeritevoliB").checked )
		{
			document.getElementById("RidPrimoAnno").checked = false;
		}
	}
	catch(err)
	{
	    
	}
}

function IseeNonPresentato ()
{
	if (document.getElementById("chkNoIsee").checked)
	{
		document.getElementById("A_ValoreIsee").value = "";
		document.getElementById("A_ValoreIsee").disabled = true;
		document.getElementById("A_ValoreIsee").placeholder = "ISEE per il diritto allo studio non presentato";
		
	}
	else
	{
		document.getElementById("A_ValoreIsee").disabled = false;
		document.getElementById("A_ValoreIsee").placeholder = "Importo ISEE per il diritto allo studio";
	}
}



//-->